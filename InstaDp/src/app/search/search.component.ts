import { Component, NgModule, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SharedServiceService } from '../sharedService/shared-service.service';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
	public query = '';
	private user_parser_regex = /com\/(\w|\W)+\?/;
	private profile_url_validator_regex = /((https:\/\/)|(http:\/\/))((www.)|())instagram.com\/(\w|\W)+\?/;
	constructor(public sharedService: SharedServiceService, private router: Router) { }

	ngOnInit() {

		const on_success = (res) => {
			this.sharedService.users = res.json().users;
			console.log(this.sharedService.users);
		}

		const on_error = (err) => {
			console.log(err)
		}

		this.sharedService.sendHttpReq(this.sharedService.url_to_load_random_profiles).subscribe(on_success, on_error);
	}
	onSearch() {
		this.router.navigate(['']);
		console.error(this.query);
		let username: string = this.parse_username_from_query_string(this.query);

		const url = this.sharedService.url_to_search_user + username;

		const on_success = (res) => {
			console.log(res);
			this.sharedService.users = res.json().users;
		}
		const on_error = (err) => {
			console.log(err);
		}
		this.sharedService.sendHttpReq(url).subscribe(on_success, on_error);
	}
	parse_username_from_query_string(query: string) {
		let username: string;
		let start_index;
		let end_index;
		if (this.profile_url_validator_regex.test(query)) {

			username = this.user_parser_regex.exec(query)[0];
			start_index = 'com/'.length;
			end_index = username.length - 2;
			username = username.substr(start_index, end_index) // removing leading [com/] and trailing [?] from the user name string;
		} else {
			username = query;
		}
		console.log(username);
		return username;
	}
}
