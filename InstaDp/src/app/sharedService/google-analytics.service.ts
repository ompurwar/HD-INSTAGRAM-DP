import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

import {environment} from '../../environments/environment';

declare var ga: Function;
@Injectable({providedIn: 'root'})
export class GoogleAnalyticsService {
  constructor(router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.url);
        ga('send', 'pageview');
      }
    });
  }
}
