import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

interface User2 {
	position: number;
	user: User;
}

interface User {
	pk: string;
	username: string;
	full_name: string;
	is_private: boolean;
	profile_pic_url: string;
	profile_pic_id?: string;
	is_verified: boolean;
	has_anonymous_profile_picture: boolean;
	follower_count: number;
	reel_auto_archive: string;
	byline: string;
	mutual_followers_count: number;
	unseen_count: number;
}

interface RootObject {
	user: User;
	status: string;
}
interface SelectedUser {
	biography: string;
	profile_pic_url: string;
	full_name: string;
	is_verified: boolean;
	follower_count: number;
	following_count: number;
	username: string;
	external_url: string;
	usertags_count: number;
	profile_pic_id: string;
	reel_auto_archive: string;
	is_private: boolean;
	media_count: number;
	has_anonymous_profile_picture: boolean;
	hd_profile_pic_url_info: Hdprofilepicurlinfo;
	external_lynx_url: string;
	has_highlight_reels: boolean;
	pk: number;
	hd_profile_pic_versions: Hdprofilepicurlinfo[];
	is_potential_business: boolean;
	auto_expand_chaining: boolean;
	highlight_reshare_disabled: boolean;
}

interface Hdprofilepicurlinfo {
	height: number;
	url: string;
	width: number;
}

@Injectable({ providedIn: 'root' })
export class SharedServiceService {
	public url_to_load_random_profiles: string;
	public url_to_search_user: string;
	public users: User2[];
	constructor(private httpReq: Http) {
		this.url_to_load_random_profiles = 'https://www.instagram.com/web/search/topsearch/?context=blended&query=d&rank_token=0.5255014413877939';
		this.url_to_search_user = 'https://www.instagram.com/web/search/topsearch/?context=blended&query=';
  }
	sendHttpReq(resourceUrl: string) { return this.httpReq.get(resourceUrl); }
}
