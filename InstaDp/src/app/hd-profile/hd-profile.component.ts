import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';

import {SharedServiceService} from '../sharedService/shared-service.service';

interface User2 {
  position: number;
  user: User;
}

interface User {
  pk: string;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id?: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  follower_count: number;
  reel_auto_archive: string;
  byline: string;
  mutual_followers_count: number;
  unseen_count: number;
}

interface RootObject {
  user: User;
  status: string;
}
interface SelectedUser {
  biography: string;
  profile_pic_url: string;
  full_name: string;
  is_verified: boolean;
  follower_count: number;
  following_count: number;
  username: string;
  external_url: string;
  usertags_count: number;
  profile_pic_id: string;
  reel_auto_archive: string;
  is_private: boolean;
  media_count: number;
  has_anonymous_profile_picture: boolean;
  hd_profile_pic_url_info: Hdprofilepicurlinfo;
  external_lynx_url: string;
  has_highlight_reels: boolean;
  pk: number;
  hd_profile_pic_versions: Hdprofilepicurlinfo[];
  is_potential_business: boolean;
  auto_expand_chaining: boolean;
  highlight_reshare_disabled: boolean;
}

interface Hdprofilepicurlinfo {
  height: number;
  url: string;
  width: number;
}
@Component({
  selector: 'app-hd-profile',
  templateUrl: './hd-profile.component.html',
  styleUrls: ['./hd-profile.component.css']
})
export class HdProfileComponent implements OnInit {
  public hd_profile_pic_url = '';
  private user_info_url_base = 'https://i.instagram.com/api/v1/users/';
  private user_info_url_end = '/info/';
  private selectedUserIndex: number;
  constructor(
      public sharedService: SharedServiceService, private route: ActivatedRoute,
      private router: Router) {}

  ngOnInit() {
    this.selectedUserIndex = this.route.snapshot.params['userId'];

    const url = this.user_info_url_base +
        this.sharedService.users[this.selectedUserIndex].user.pk +
        this.user_info_url_end;
    this.hd_profile_pic_url =
        this.sharedService.users[this.selectedUserIndex].user.profile_pic_url;
    this.sharedService.sendHttpReq(url).subscribe(
        (res) => {
          const userInfo: SelectedUser = res.json().user;
          console.log(userInfo);
          console.log(this.hd_profile_pic_url);
          this.hd_profile_pic_url =
              userInfo.hd_profile_pic_url_info.url;
          console.log(userInfo);
        },
        (err) => { console.log(err); });
  }
  onShare() {
    window.location.assign(
        'https://wa.me/?text=' + this.hd_profile_pic_url);
  }
  back() { this.router.navigate(['']); }
}
