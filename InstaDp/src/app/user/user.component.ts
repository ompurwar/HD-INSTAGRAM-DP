import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {browser} from 'protractor';
import {Window} from 'selenium-webdriver';

import {SharedServiceService} from '../sharedService/shared-service.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  constructor(
      public sharedService: SharedServiceService, private router: Router) {}

  ngOnInit() {
  }
  // invoke the function when user user pic is selected
  onSelect(userIndex: number) {
    this.router.navigate(['hd-profile', userIndex]);

    console.log('selected user at index:\t' + userIndex);
  }
}
