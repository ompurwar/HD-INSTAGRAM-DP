import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'How to download instagram profile pic in HD?';
  pageUrl = 'https://dp-instagram.firebaseapp.com/';
  description = `\n
  Questions:
  How to\n
  1: Download instagram profile pic of private account ?
  2: Download instagram profile pic in hd
  3: Download instagram profile app
  4: Download instagram profile dp
  5: Download instagram profile data
  6: Download instagram profile full size
  7: How to download instagram profile pic in hd ?
  8: Download instagram profile image
  9: Download instagram profile online
  10: Download instagram profile picture hd
  11: Download instagram profile picture online
  12: Download instagram profile picture app
  13: Download instagram profile picture 2018
  14: Download instagram profile picture android
  15: Download instagram profile picture url
  16: Download instagram profile picture apk
  17: Download instagram profile story
  18: Download instagram profile viewer
  19: Download instagram profile viewer ver 1.5
  20: Download instagram profile video
  21: Download instagram profile viewer apk\n

  Answer:
  1: Go to https://dp-instagram.firebaseapp.com/ \n
  2: Then serach the exact username on the search bar.\n
  3: Now you will see the list of private profile pictures will appear\n
  4: Select the desired picture and wait it may take few seconds depending on your connection speed\n
  5: Now you can see the enlarged HD Image\n
  6: long press on image (on cell-phones)/right click (on desktop)\n
  7: click save Image\n
  8: Or you can directly share using whatsapp by pressing share button\n`;
  constructor(private _title: Title, private _meta: Meta) {}
  ngOnInit() {
    this._title.setTitle(this.title);
    this._meta.updateTag({name: 'description', content: this.description});
    this._meta.updateTag({name: 'url', content: this.pageUrl});
    this._meta.updateTag({property: 'og:Title', content: this.title});
    this._meta.updateTag(
        {property: 'og:description', content: this.description});
    this._meta.updateTag({property: 'og:url', content: this.pageUrl});
    this._meta.updateTag({property: 'og:type', content: 'website'});
  }
}
