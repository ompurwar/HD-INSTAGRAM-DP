import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {HdProfileComponent} from './hd-profile/hd-profile.component';
import {SearchComponent} from './search/search.component';
import {GoogleAnalyticsService} from './sharedService/google-analytics.service';
import {SharedServiceService} from './sharedService/shared-service.service';
import {UserComponent} from './user/user.component';

const appRoutes: Routes = [
  {path: '', component: UserComponent},
  {path: 'hd-profile/:userId', component: HdProfileComponent}
];
@NgModule({
  declarations:
      [AppComponent, UserComponent, SearchComponent, HdProfileComponent],
  imports:
      [BrowserModule, HttpModule, FormsModule, RouterModule.forRoot(appRoutes)],
  providers: [
    SharedServiceService,
    GoogleAnalyticsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(protected _googleAnalyticsService: GoogleAnalyticsService) {}
}
